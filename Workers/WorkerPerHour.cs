﻿namespace Workers
{
    public class WorkerPerHour : AWorker
    {
        WorkerPerHour() { }

        public WorkerPerHour(int id, string name, decimal salary)
        {
            base.ID = id;
            base.Name = name;
            base.Salary = salary;
        }

        public override decimal GetAveragePerMonth()
        {
            return base.Salary * (decimal)(20.8 * 8);
        }
    }
}
