﻿using System;
using System.Xml.Serialization;
namespace Workers
{
    [Serializable]
    [XmlInclude(typeof(WorkerFixed))]
    [XmlInclude(typeof(WorkerPerHour))]
    public abstract class AWorker
    {
        private int id;
        private string name;
        private decimal salary;

        public AWorker() {}

        public AWorker(int id, string name, decimal salary)
        {
            this.id = id;
            this.name = name;
            this.salary = salary;
        }

        abstract public decimal GetAveragePerMonth();

        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public decimal Salary
        {
            get { return this.salary; }
            set { this.salary = value; }
        }
    }
}
