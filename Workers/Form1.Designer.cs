﻿namespace Workers
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.path_tb = new System.Windows.Forms.TextBox();
            this.open_btn = new System.Windows.Forms.Button();
            this.save_btn = new System.Windows.Forms.Button();
            this.create_bt = new System.Windows.Forms.Button();
            this.N_tb = new System.Windows.Forms.TextBox();
            this.names_dgv = new System.Windows.Forms.DataGridView();
            this.ids_dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.Nnames_tb = new System.Windows.Forms.TextBox();
            this.Nids_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.select_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.names_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ids_dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // path_tb
            // 
            this.path_tb.Location = new System.Drawing.Point(13, 13);
            this.path_tb.Name = "path_tb";
            this.path_tb.Size = new System.Drawing.Size(250, 20);
            this.path_tb.TabIndex = 0;
            // 
            // open_btn
            // 
            this.open_btn.Location = new System.Drawing.Point(269, 11);
            this.open_btn.Name = "open_btn";
            this.open_btn.Size = new System.Drawing.Size(56, 23);
            this.open_btn.TabIndex = 1;
            this.open_btn.Text = "Open";
            this.open_btn.UseVisualStyleBackColor = true;
            this.open_btn.Click += new System.EventHandler(this.open_btn_Click);
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(331, 11);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(53, 23);
            this.save_btn.TabIndex = 2;
            this.save_btn.Text = "Save";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // create_bt
            // 
            this.create_bt.Location = new System.Drawing.Point(509, 11);
            this.create_bt.Name = "create_bt";
            this.create_bt.Size = new System.Drawing.Size(75, 23);
            this.create_bt.TabIndex = 3;
            this.create_bt.Text = "Create";
            this.create_bt.UseVisualStyleBackColor = true;
            this.create_bt.Click += new System.EventHandler(this.create_bt_Click);
            // 
            // N_tb
            // 
            this.N_tb.Location = new System.Drawing.Point(590, 13);
            this.N_tb.Name = "N_tb";
            this.N_tb.Size = new System.Drawing.Size(65, 20);
            this.N_tb.TabIndex = 4;
            this.N_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.N_tb_KeyPress);
            // 
            // names_dgv
            // 
            this.names_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.names_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.names_dgv.Location = new System.Drawing.Point(12, 128);
            this.names_dgv.Name = "names_dgv";
            this.names_dgv.Size = new System.Drawing.Size(313, 268);
            this.names_dgv.TabIndex = 5;
            // 
            // ids_dgv
            // 
            this.ids_dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ids_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ids_dgv.Location = new System.Drawing.Point(342, 128);
            this.ids_dgv.Name = "ids_dgv";
            this.ids_dgv.Size = new System.Drawing.Size(313, 268);
            this.ids_dgv.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(596, 36);
            this.label1.MaximumSize = new System.Drawing.Size(60, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "Number of workers";
            // 
            // Nnames_tb
            // 
            this.Nnames_tb.Location = new System.Drawing.Point(13, 102);
            this.Nnames_tb.Name = "Nnames_tb";
            this.Nnames_tb.Size = new System.Drawing.Size(51, 20);
            this.Nnames_tb.TabIndex = 8;
            this.Nnames_tb.Text = "5";
            this.Nnames_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nnames_tb_KeyPress);
            // 
            // Nids_tb
            // 
            this.Nids_tb.Location = new System.Drawing.Point(342, 102);
            this.Nids_tb.Name = "Nids_tb";
            this.Nids_tb.Size = new System.Drawing.Size(51, 20);
            this.Nids_tb.TabIndex = 9;
            this.Nids_tb.Text = "3";
            this.Nids_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nids_tb_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "N of first names";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(399, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "N of last IDs";
            // 
            // select_btn
            // 
            this.select_btn.Location = new System.Drawing.Point(269, 40);
            this.select_btn.Name = "select_btn";
            this.select_btn.Size = new System.Drawing.Size(115, 23);
            this.select_btn.TabIndex = 12;
            this.select_btn.Text = "Select data";
            this.select_btn.UseVisualStyleBackColor = true;
            this.select_btn.Click += new System.EventHandler(this.select_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 408);
            this.Controls.Add(this.select_btn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Nids_tb);
            this.Controls.Add(this.Nnames_tb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ids_dgv);
            this.Controls.Add(this.names_dgv);
            this.Controls.Add(this.N_tb);
            this.Controls.Add(this.create_bt);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.open_btn);
            this.Controls.Add(this.path_tb);
            this.Name = "Form1";
            this.Text = "Workers";
            ((System.ComponentModel.ISupportInitialize)(this.names_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ids_dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox path_tb;
        private System.Windows.Forms.Button open_btn;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button create_bt;
        private System.Windows.Forms.TextBox N_tb;
        private System.Windows.Forms.DataGridView names_dgv;
        private System.Windows.Forms.DataGridView ids_dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Nnames_tb;
        private System.Windows.Forms.TextBox Nids_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button select_btn;
    }
}

