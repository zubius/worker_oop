﻿namespace Workers
{
    public class WorkerFixed : AWorker
    {
        WorkerFixed() { }

        public WorkerFixed(int id, string name, decimal salary)
        {
            base.ID = id;
            base.Name = name;
            base.Salary = salary;
        }

        public override decimal GetAveragePerMonth()
        {
            return base.Salary;
        }
    }
}
