﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Workers
{
    class ManipulateWorkers
    {
        public AWorker[] workers;
        private XmlSerializer serializer = new XmlSerializer(typeof(AWorker[]));

        /// <summary>
        /// Create N workers, 1/2 of em with fixed salary, 1/2 with per hour
        /// </summary>
        /// <param name="N">Number of workers</param>
        public ManipulateWorkers(int N)
        {
            workers = new AWorker[N];

            int i = 0;
            for (; i < N / 2; i++)
            {
                workers[i] = new WorkerFixed(i, i + "f", i + 10);
            }

            for (; i < N; i++)
            {
                workers[i] = new WorkerPerHour(i, i + "ph", i + 1);
            }
        }

        /// <summary>
        /// Load workers from file
        /// </summary>
        /// <param name="path">Path to the .xml file with workers</param>
        public ManipulateWorkers(string path)
        {
            ReadFromFile(path);
        }

        public void SortWorkers()
        {
            workers = (from w in workers
                       orderby w.GetAveragePerMonth() descending, w.Name
                       select w).ToArray();
        }

        public List<string> FirstNNames(int N)
        {
            List<string> names = new List<string>();

            var n = workers.Take(N);
            foreach (AWorker aw in n)
            {
                names.Add(aw.Name);
            }

            return names;
        }

        public List<int> LastNID(int N)
        {
            List<int> ids = new List<int>();

            var n = workers.Skip(workers.Length - N);
            foreach (AWorker aw in n)
            {
                ids.Add(aw.ID);
            }

            return ids;
        }

        public void SaveToFile(string filename)
        {
            if (!String.IsNullOrEmpty(filename))
            {
                using (TextWriter tw = new StreamWriter(filename))
                {
                    serializer.Serialize(tw, workers);
                    tw.Close();
                }
            }
        }

        public void ReadFromFile(string filename)
        {
            using (StreamReader sr = new StreamReader(filename))
            {
                workers = (AWorker[])serializer.Deserialize(sr);
            }
        }
    }
}
