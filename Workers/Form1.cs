﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Workers
{
    public partial class Form1 : Form
    {
        private string path = "WorkersList.xml";
        ManipulateWorkers mw;

        public Form1()
        {
            InitializeComponent();
            path_tb.Text = path;

            DoWorkers(15);
        }

        /// <summary>
        /// Generating N new workers
        /// </summary>
        /// <param name="N">Number of workers</param>
        private void DoWorkers(int N)
        {
            mw = new ManipulateWorkers(N);

            mw.SortWorkers();

            FillDGV(mw, Convert.ToInt32(Nnames_tb.Text), Convert.ToInt32(Nids_tb.Text));
        }

        /// <summary>
        /// Getting early stored in file workers
        /// </summary>
        /// <param name="path">Path to .xml file</param>
        private void DoWorkers(string path)
        {
            mw = new ManipulateWorkers(path);

            mw.SortWorkers();

            FillDGV(mw, Convert.ToInt32(Nnames_tb.Text), Convert.ToInt32(Nids_tb.Text));
        }

        /// <summary>
        /// Fill DataGridView
        /// </summary>
        /// <param name="mw">Manipulate with workers object</param>
        /// <param name="nnames">number of first names to select</param>
        /// <param name="nids">number of last IDs to select</param>
        private void FillDGV(ManipulateWorkers mw, int nnames, int nids)
        {
            names_dgv.DataSource = mw.FirstNNames(nnames).Select(x => new { Names = x }).ToList();
            ids_dgv.DataSource = mw.LastNID(nids).Select(x => new { IDs = x }).ToList();
        }

        private void open_btn_Click(object sender, EventArgs e)
        {
            if (!path_tb.Text.EndsWith(".xml"))
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                openDialog.DefaultExt = ".xml";
                openDialog.Filter = "XML File|*.xml";
                DialogResult result = openDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    path = openDialog.FileName;
                    path_tb.Text = path;
                }
            }

            DoWorkers(path);
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (!path_tb.Text.EndsWith(".xml"))
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.DefaultExt = ".xml";
                saveDialog.Filter = "XML File|*.xml";
                DialogResult result = saveDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    path = saveDialog.FileName;
                    path_tb.Text = path;
                }
            }

            mw.SaveToFile(path);
        }

        private void N_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')
            {
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void create_bt_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(N_tb.Text))
                DoWorkers(Convert.ToInt32(N_tb.Text));
        }

        private void Nnames_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')
            {
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void Nids_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')
            {
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void select_btn_Click(object sender, EventArgs e)
        {
            FillDGV(mw, Convert.ToInt32(Nnames_tb.Text), Convert.ToInt32(Nids_tb.Text));
        }
    }
}
